<?php
declare(strict_types=1);
namespace OCA\SSEPush\AppInfo;

return [
	'routes' => [
		[
			'name' => 'SSEPush#ssesub',
			'url' => '/sse/',
			'verb' => 'POST',
		],
		[
			'name' => 'SSEPush#notify',
			'url' => '/notifications/',
			'verb' => 'POST',
		],
		[
			'name' => 'SSEPush#external',
			'url' => '/external/',
			'verb' => 'POST',
		],
		[
			'name' => 'SSEPush#authorize',
			'url' => '/authorize/',
			'verb' => 'POST',
		],
		[
			'name' => 'SSEPush#genauth',
			'url' => '/genauth/{serv_name}',
			'verb' => 'GET',
		],
		[
			'name' => 'SSEPush#rmauth',
			'url' => '/rmauth/{token}',
			'verb' => 'GET',
		],
		[
			'name' => 'SSEPush#setkeepalive',
			'url' => '/setkeepalive/{value}',
			'verb' => 'GET',
		],
	]
];
