<?php
namespace OCA\SSEPush;

use OCP\IURLGenerator;
use OCP\Notification\INotification;
use OCP\Notification\INotifier;

class Notifier implements INotifier {

	/**
	 * Identifier of the notifier
	 *
	 * @return string
	 */
	public function getID(): string {
		return 'ssepush';
	}

	/**
	 * Human readable name describing the notifier
	 *
	 * @return string
	 */
	public function getName(): string {
		return 'Notification Push';
	}

	/**
	 * @param INotification $notification
	 * @param string $languageCode The code of the language that should be used to prepare the notification
	 * @return INotification
	 * @throws \InvalidArgumentException When the notification was not prepared by a notifier
	 */
	public function prepare(INotification $notification, string $languageCode): INotification {
		if (strpos($notification->getApp(), 'ssepushx-') !== 0) {
			throw new \InvalidArgumentException();
		}

		$subject = array();
		$subject['headers'] = $notification->getSubjectParameters();
		$subject['data'] = $notification->getMessageParameters();

		$notification->setRichSubject("SSEPush ".$notification->getSubject()."\n".$notification->getMessage()); // This is the notification shown inside nextcloud
		$notification->setParsedSubject($notification->getSubject()."\n".$notification->getMessage()); // This is the data sent to push-v2

		return $notification;
	}
}
