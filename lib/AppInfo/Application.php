<?php
declare(strict_types=1);

namespace OCA\SSEPush\AppInfo;

use OCA\SSEPush\Capabilities;
use OCA\SSEPush\Notifier;
use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;

class Application extends App implements IBootstrap {

	public function __construct() {
		parent::__construct('ssepush');
	}

	public function register(IRegistrationContext $context): void {
		$context->registerCapability(Capabilities::class);
	}

	public function boot(IBootContext $context): void {
		$context->getServerContainer()->getNotificationManager()->registerNotifierService(Notifier::class);
	}
}
